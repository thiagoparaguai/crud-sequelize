const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


const authConfig = require('../../config/auth.json');

const { User } = require('../models/index');

const router = express.Router();

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

router.post('/register', async (req, res) => {
  const { email } = req.body;

  try {
    if (await User.findOne({ where: { email: email } }))
      return res.status(400).send({ error: 'E-mail already registered' });

    const user = await User.create(req.body);
    user.password = undefined;
    return res.status(200)
      .send({
        user,
        token: generateToken({ id: user.id }),
      });
  } catch (err) {
    return res.status(400).send({ error: 'Registration failed' });
  }
});

router.post('/authenticate', async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ where: { email: email } });

  if (!user)
    return res.status(400).send({ error: 'User not found' });

  if (!await bcrypt.compareSync(password, user.password))
    return res.status(400).send({ error: 'Invalid password' });

  user.password = undefined;

  res.send({
    user,
    token: generateToken({ id: user.id }),
  });

});


module.exports = app => app.use('/sign', router);