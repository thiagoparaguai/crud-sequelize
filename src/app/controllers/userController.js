const express = require('express');
const authMiddleware = require('../middlewares/auth');

const { User } = require('../models/index');

const router = express.Router();
router.use(authMiddleware); // Com autenticação

router.get('/all', async (req, res) => {
  try {
    const users = await User.all({
      attributes: {
        exclude: ['password']
      }
    });
    return res.status(200).send({ users });
  } catch (err) {
    return res.status(400).send({ error: 'Erro loading users' })
  }
});


module.exports = app => app.use('/user', router);