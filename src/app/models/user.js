const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      select: false,
    }
  });

  function generateHash(user) {
    if (user === null) {
      throw new Error('No  found employee');
    }
    else if (!user.changed('password')) return user.password;
    else {
      let salt = bcrypt.genSaltSync(10);
      return user.password = bcrypt.hashSync(user.password, salt);
    }
  }

  User.beforeCreate(generateHash);

  User.beforeUpdate(generateHash);

  return User;
}