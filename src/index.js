const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Imports Controllers and Routes
require('./app/controllers/index')(app);


const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.listen(port);
// app.on('error', onError);
// app.on('listening', onListening);
console.log('Api rodando na porta ' + port);







// Função para definir a porta
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

// Tratar erros e informação
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string ' ?
    'Pipe ' + port :
    'Port ' + port;

  switch (error.code) {
    case 'EACESS':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// Função para Listar Debug
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ?
    'Pipe ' + addr :
    'Port ' + addr.port;
  debug('Listening on ' + bind);
}